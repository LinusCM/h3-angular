import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { PersonComponent } from "./person/person.component";

@Component({
  selector: "app-root",
  standalone: true,
  imports: [RouterOutlet, PersonComponent],
  templateUrl: "./app.component.html",
  styleUrl: "./app.component.scss",
})
export class AppComponent {
  title = "your-schoolmate";
}
