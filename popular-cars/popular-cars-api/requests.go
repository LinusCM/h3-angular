package main

import (
	"errors"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func login(ctx *gin.Context) {
	// WARNING: This is a temp solution, just to get things working.
	// You should never store credentials in your code.
	passwords := map[string]string{
		"admin": "goodpassword",
		"user":  "badpassword",
	}

	var user Login_Request

	if err := ctx.BindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"Invalid login form.": err.Error()})
		return
	}

	// Check if username exists and fetch password
	fetchedPassword, ok := passwords[user.Username]

	// Auth wrong
	if !ok || fetchedPassword != user.Password {
		ctx.JSON(http.StatusUnauthorized, gin.H{})
		return
	}

	// Return token
	ctx.JSON(http.StatusOK, gin.H{
		"token":     generateJWT(user.Username),
		"username ": user.Username,
	})
}

func authenticate(ctx *gin.Context) (string, error) {
	var payload Payload

	// Check validity of header
	auth := ctx.Request.Header["Authorization"]

	//Header wrong
	if len(auth) < 1 {
		ctx.JSON(http.StatusUnauthorized, gin.H{})
		return "", errors.New("No auth header passed.")
	}

	// Check auth scheme
	authParts := strings.Split(auth[0], " ")

	if authParts[0] != "Bearer" || len(authParts) != 2 {
		ctx.JSON(http.StatusBadRequest, gin.H{})
		return "", errors.New("Auth scheme is invalid.")
	}

	// Auth wrong
	payload, err := authenticateJWT(authParts[1])
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{})
		return payload.User, errors.New("Auth is invalid.")
	}

	return payload.User, nil
}

func delete_car(ctx *gin.Context) {
	fetched_rank, err := strconv.Atoi(ctx.Param("rank"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, "Rank invalid.")
	}

	// Check if rank is out of bounds.
	if fetched_rank >= 0 && fetched_rank > len(car_rankings)+2 {
		ctx.JSON(http.StatusBadRequest, "Car rank is out of bounds.")
		return
	}

	// Delete entry from car_entries.
	for i, car := range car_rankings {
		if car.Rank == fetched_rank {
			car_rankings = append(car_rankings[:i], car_rankings[i+1:]...)
			break
		}
	}

	// Update the rankings of leftover entries.
	for i, car := range car_rankings {
		if fetched_rank > 0 && car.Rank >= fetched_rank {
			car_rankings[i].Rank = car.Rank - 1
		}
	}

	// Send back removed entry for confirmation.
	ctx.JSON(http.StatusNoContent, "")
}

func add_car(ctx *gin.Context) {
	var new_car Car_Entry
	var new_car_request Car_Entry_Request

	// Map the values to the Car_Entry model.
	if err := ctx.BindJSON(&new_car_request); err != nil {
		ctx.JSON(http.StatusInternalServerError, "")
		return
	}

	// Convert values
	entry, err := convert_json_to_entry(new_car_request)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, "Could not convert values.")
		return
	}

	new_car = entry

	// Check if rank is out of bounds.
	if new_car.Rank >= 0 && new_car.Rank > len(car_rankings)+3 {
		ctx.JSON(http.StatusBadRequest, "Car rank is out of index.")
		return
	}

	// Update the rankings of existing entries.
	for i, car := range car_rankings {
		if car.Rank >= new_car.Rank && new_car.Rank > 0 {
			car_rankings[i].Rank = car.Rank + 1
		}
	}

	// Send back created (201) &
	// update car_entries.
	car_rankings = append(car_rankings, new_car)
	ctx.JSON(http.StatusCreated, new_car)

	// Sort the slice.
	sort.Slice(car_rankings, func(i, j int) bool {
		return car_rankings[i].Rank < car_rankings[j].Rank
	})
}
