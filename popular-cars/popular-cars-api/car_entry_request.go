package main

type Car_Entry_Request struct {
	Rank                    string `json:"rank"`
	Model                   string `json:"model"`
	Quantity                string `json:"quantity"`
	PopularityChangePercent string `json:"popularityChangePercent"`
}
