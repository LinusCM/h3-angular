package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Basic database of cars
var car_rankings []Car_Entry

func main() {
	car_rankings = []Car_Entry{}

	router := gin.Default()

	router.Use(cors_middleware())

	// Login & JWT
	router.POST("login", func(ctx *gin.Context) {
		login(ctx)
	})

	// Fetch & manage cars
	router.GET("cars", func(ctx *gin.Context) {
		_, err := authenticate(ctx)
		if err != nil {
			ctx.JSON(http.StatusUnauthorized, gin.H{})
			return
		}

		ctx.JSON(http.StatusOK, car_rankings)
	})

	router.POST("cars", func(ctx *gin.Context) {
		// Authenticate if the user is an admin
		user, err := authenticate(ctx)
		if err != nil || user != "admin" {
			ctx.JSON(http.StatusUnauthorized, gin.H{})
			return
		}

		// Then add
		add_car(ctx)
	})

	router.DELETE("cars/:rank", func(ctx *gin.Context) {
		// Authenticate if the user is an admin
		user, err := authenticate(ctx)
		if err != nil || user != "admin" {
			ctx.JSON(http.StatusUnauthorized, gin.H{})
			return
		}

		// Then remove
		delete_car(ctx)
	})

	router.Run(":8080")
}
