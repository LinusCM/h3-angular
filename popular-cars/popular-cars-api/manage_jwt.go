package main

import (
	"errors"
	"log"

	"github.com/golang-jwt/jwt/v5"
)

// WARNING: This is a temp solution, just to get things working.
// You should never store keys in your code.
var key = []byte("ThisIsAGoodKEy")

type Payload struct {
	User string `json:"user"`
	jwt.RegisteredClaims
}

func generateJWT(username string) string {
	payload := Payload{
		username,
		jwt.RegisteredClaims{},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)

	// We never want signing to fail
	tokenString, err := token.SignedString(key)
	if err != nil {
		log.Fatal(err)
	}

	return tokenString
}

func authenticateJWT(tokenString string) (Payload, error) {
	// jwt.Parse takes two arguments, the latter being an anonymous function
	// the tokenString gets parsed and tossed into the anonymous function.
	// In the anonymous function, the key is found.
	// We don't care, as we have one key and that one gets returned.
	token, err := jwt.ParseWithClaims(tokenString, &Payload{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	if err != nil {
		return *new(Payload), errors.New("Can't parse given token.")
	}

	// Casting to Payload struct
	payload, ok := token.Claims.(*Payload)

	if ok && token.Valid {
		return *payload, nil
	}

	return *new(Payload), errors.New("Payload and/or token is invalid.")
}
