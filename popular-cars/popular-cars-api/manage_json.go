package main

import (
	"fmt"
	"strconv"
)

func convert_json_to_entry(request Car_Entry_Request) (Car_Entry, error) {
	var car_entry Car_Entry

	rank, err := strconv.Atoi(request.Rank)
	if err != nil {
		return car_entry, fmt.Errorf("failed to convert string to int: %w", err)
	}
	quantity, err := strconv.Atoi(request.Rank)
	if err != nil {
		return car_entry, fmt.Errorf("failed to convert string to int: %w", err)
	}
	popularity, err := strconv.Atoi(request.Rank)
	if err != nil {
		return car_entry, fmt.Errorf("failed to convert string to int: %w", err)
	}

	car_entry.Rank = rank
	car_entry.Model = request.Model
	car_entry.Quantity = quantity
	car_entry.PopularityChangePercent = popularity

	return car_entry, nil
}
