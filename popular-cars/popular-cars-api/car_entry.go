package main

type Car_Entry struct {
	Rank                    int    `json:"rank"`
	Model                   string `json:"model"`
	Quantity                int    `json:"quantity"`
	PopularityChangePercent int    `json:"popularityChangePercent"`
}
