import { Component } from "@angular/core";
import { CarRankingsTableComponent } from "../car-rankings-table/car-rankings-table.component";
import { AddCarFormComponent } from "../add-car-form/add-car-form.component";
import { RemoveCarComponent } from "../remove-car/remove-car.component";

@Component({
  selector: "app-car-rankings-page",
  standalone: true,
  imports: [CarRankingsTableComponent, AddCarFormComponent, RemoveCarComponent],
  templateUrl: "./car-rankings-page.component.html",
  styleUrl: "./car-rankings-page.component.scss",
})
export class CarRankingsPageComponent {}
