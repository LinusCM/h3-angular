import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarRankingsPageComponent } from './car-rankings-page.component';

describe('CarRankingsPageComponent', () => {
  let component: CarRankingsPageComponent;
  let fixture: ComponentFixture<CarRankingsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CarRankingsPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CarRankingsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
