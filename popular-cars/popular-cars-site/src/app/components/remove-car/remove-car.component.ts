import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CarRankingsService } from "../../services/car-rankings.service";

import { MatFormField } from "@angular/material/form-field";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-remove-car",
  standalone: true,
  imports: [MatFormField, MatButtonModule, MatInputModule, ReactiveFormsModule],
  templateUrl: "./remove-car.component.html",
  styleUrl: "./remove-car.component.scss",
})
export class RemoveCarComponent {
  carForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private carRankingsService: CarRankingsService,
    private snackBar: MatSnackBar,
  ) {
    this.carForm = this.formBuilder.group({
      rank: [null, Validators.required],
    });
  }

  onSubmit(): void {
    if (this.carForm.valid) {
      const fetched_rank = this.carForm.get("rank")!.value as number;

      this.carRankingsService.removeCar(fetched_rank).subscribe(
        (response) => {
          this.carRankingsService.fetchCars();
        },
        (error) => {
          this.snackBar.open("Invalid request. Please try again.", "Dismiss", {
            duration: 3000,
            horizontalPosition: "center",
            verticalPosition: "bottom",
          });
        },
      );
    }
  }
}
