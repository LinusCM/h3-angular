import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CarEntry } from "../../interfaces/car-entry";
import { CarRankingsService } from "../../services/car-rankings.service";
import { MatFormField, MatLabel } from "@angular/material/form-field";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-add-car-form",
  standalone: true,
  imports: [MatFormField, MatLabel, ReactiveFormsModule, MatInputModule, MatButtonModule],
  templateUrl: "./add-car-form.component.html",
  styleUrl: "./add-car-form.component.scss",
})
export class AddCarFormComponent {
  carForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private carRankingsService: CarRankingsService,
    private snackBar: MatSnackBar,
  ) {
    this.carForm = this.formBuilder.group({
      // Missing regex, no time
      rank: ["", Validators.required],
      model: ["", Validators.required],
      quantity: ["", Validators.required],
      popularityChangePercent: ["", Validators.required],
    });
  }

  onSubmit(): void {
    if (this.carForm.valid) {
      const newCar = this.carForm.value as CarEntry;

      this.carRankingsService.addCar(newCar).subscribe(
        (response) => {
          this.carRankingsService.fetchCars();
        },
        (error) => {
          this.snackBar.open("Invalid request. Please try again.", "Dismiss", {
            duration: 3000,
            horizontalPosition: "center",
            verticalPosition: "bottom",
          });
        },
      );
    }
  }
}
