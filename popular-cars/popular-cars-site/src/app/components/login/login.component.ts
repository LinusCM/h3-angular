import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormField, MatLabel } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { LoginService } from "../../services/login.service";
import { Credentials } from "../../interfaces/credentials";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormField,
    MatLabel,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
  ],
  templateUrl: "./login.component.html",
  styleUrl: "./login.component.scss",
})
export class LoginComponent {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  onLogin(): void {
    if (this.loginForm.valid) {
      const newLogin = this.loginForm.value as Credentials;

      this.loginService.login(newLogin).subscribe(
        (response) => {
          localStorage.setItem("token", response.token);

          if (this.loginService.isLoggedInAdmin()) {
            this.router.navigate(["/adjust-rankings"]);
          } else {
            this.router.navigate(["/rankings"]);
          }
        },
        (error) => {
          this.snackBar.open("Login failed. Please check your credentials.", "Dismiss", {
            duration: 3000,
            horizontalPosition: "center",
            verticalPosition: "bottom",
          });
        },
      );
    }
  }
}
