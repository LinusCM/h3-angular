import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarRankingsTableComponent } from './car-rankings-table.component';

describe('CarRankingsTableComponent', () => {
  let component: CarRankingsTableComponent;
  let fixture: ComponentFixture<CarRankingsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CarRankingsTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CarRankingsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
