import { Component } from "@angular/core";
import { CarRankingsService } from "../../services/car-rankings.service";
import { CarEntry } from "../../interfaces/car-entry";
import { MatTableDataSource, MatTableModule } from "@angular/material/table";

@Component({
  selector: "app-car-rankings-table",
  standalone: true,
  imports: [MatTableModule],
  templateUrl: "./car-rankings-table.component.html",
  styleUrl: "./car-rankings-table.component.scss",
})
export class CarRankingsTableComponent {
  dataSource: MatTableDataSource<CarEntry> = new MatTableDataSource<CarEntry>();
  displayedColumns: string[] = ["rank", "model", "quantity", "popularityChangePercent"];

  constructor(private carRankingService: CarRankingsService) {}

  ngOnInit(): void {
    this.carRankingService.fetchCars();

    this.carRankingService.popularcCars$.subscribe((cars) => {
      this.dataSource.data = cars;
    });
  }
}
