import { Routes } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { CarRankingsPageComponent } from "./components/car-rankings-page/car-rankings-page.component";
import { CarRankingsTableComponent } from "./components/car-rankings-table/car-rankings-table.component";
import { authGuard } from "./guards/auth.guard";

export const routes: Routes = [
  { path: "signin", component: LoginComponent },
  { path: "rankings", component: CarRankingsTableComponent },
  { path: "adjust-rankings", component: CarRankingsPageComponent, canActivate: [authGuard] },
  { path: "", redirectTo: "/signin", pathMatch: "full" },
];
