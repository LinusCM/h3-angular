import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, BehaviorSubject, Subject } from "rxjs";
import { CarEntry } from "../interfaces/car-entry";

@Injectable({
  providedIn: "root",
})
export class CarRankingsService {
  // We do not want anyone to update our list, so we keep our subject/entries private.
  private popularCars: Array<CarEntry> = [];
  private popularCarsSubject$: Subject<CarEntry[]> = new BehaviorSubject<CarEntry[]>(
    this.popularCars,
  );

  // Can only be subscribed to, as the subject is now an observable.
  popularcCars$: Observable<CarEntry[]> = this.popularCarsSubject$.asObservable();

  constructor(private httpClient: HttpClient) {}

  fetchCars(): void {
    // Something like an intercepter would work,
    // however lacking time.
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    });

    this.httpClient.get<CarEntry[]>("/cars", { headers }).subscribe((cars) => {
      this.popularCars = cars;

      this.popularCarsSubject$.next(this.popularCars);
    });
  }

  addCar(carEntry: CarEntry): Observable<CarEntry> {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    });

    return this.httpClient.post<CarEntry>("/cars", carEntry, { headers });
  }

  removeCar(carRank: number): Observable<CarEntry> {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    });

    return this.httpClient.delete<CarEntry>(`/cars/${carRank}`, { headers });
  }
}
