import { TestBed } from '@angular/core/testing';

import { CarRankingsService } from './car-rankings.service';

describe('CarRankingsService', () => {
  let service: CarRankingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarRankingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
