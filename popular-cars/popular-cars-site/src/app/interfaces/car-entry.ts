export interface CarEntry {
  rank: string | null;
  model: string | null;
  quantity: string | null;
  popularityChangePercent: string | null;
}
