export interface GeometryCalculator {
  calculatePerimeter(...args: number[]): number;

  calculateArea(...args: number[]): number;
}
