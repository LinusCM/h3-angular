import { Injectable } from "@angular/core";
import { GeometryCalculator } from "../interfaces/geometry-calculator";

@Injectable({
  providedIn: "root",
})
export class RectangleCalculatorService implements GeometryCalculator {
  calculatePerimeter(width: number, length: number): number {
    return 2 * (width + length);
  }

  calculateArea(width: number, length: number): number {
    return width * length;
  }
}
