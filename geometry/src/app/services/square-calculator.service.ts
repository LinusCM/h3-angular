import { Injectable } from "@angular/core";
import { GeometryCalculator } from "../interfaces/geometry-calculator";

@Injectable({
  providedIn: "root",
})
export class SquareCalculatorService implements GeometryCalculator {
  calculatePerimeter(sideLength: number): number {
    return 4 * sideLength;
  }

  calculateArea(sideLength: number): number {
    return sideLength * sideLength;
  }
}
