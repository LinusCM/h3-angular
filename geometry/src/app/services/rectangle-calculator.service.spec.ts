import { TestBed } from '@angular/core/testing';

import { RectangleCalculatorService } from './rectangle-calculator.service';

describe('RectangleCalculatorService', () => {
  let service: RectangleCalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RectangleCalculatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
