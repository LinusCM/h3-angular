import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SquareInputFormComponent } from './square-input-form.component';

describe('SquareInputFormComponent', () => {
  let component: SquareInputFormComponent;
  let fixture: ComponentFixture<SquareInputFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SquareInputFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SquareInputFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
