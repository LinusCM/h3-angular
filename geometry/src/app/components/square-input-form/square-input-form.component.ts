import { Component } from "@angular/core";
import { SquareCalculatorService } from "../../services/square-calculator.service";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

@Component({
  selector: "app-square-input-form",
  standalone: true,
  imports: [
    MatFormFieldModule,
    FormsModule,
    CommonModule,
    MatButtonModule,
    MatInputModule,
  ],
  templateUrl: "./square-input-form.component.html",
  styleUrl: "./square-input-form.component.scss",
})
export class SquareInputFormComponent {
  side: number | null = null;
  perimeter: number | null = null;
  area: number | null = null;

  constructor(private rectangleCalculatorService: SquareCalculatorService) {}

  calculatePerimeter(): void {
    if (this.side !== null) {
      this.perimeter = this.rectangleCalculatorService.calculatePerimeter(
        this.side,
      );
    }
  }

  calculateArea(): void {
    if (this.side !== null) {
      this.area = this.rectangleCalculatorService.calculateArea(this.side);
    }
  }

  calculateAll(): void {
    this.calculatePerimeter();
    this.calculateArea();
  }
}
