import { Component } from "@angular/core";
import { RectangleCalculatorService } from "../../services/rectangle-calculator.service";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

@Component({
  selector: "app-rectangle-input-form",
  standalone: true,
  imports: [
    MatFormFieldModule,
    FormsModule,
    CommonModule,
    MatButtonModule,
    MatInputModule,
  ],
  templateUrl: "./rectangle-input-form.component.html",
  styleUrl: "./rectangle-input-form.component.scss",
})
export class RectangleInputFormComponent {
  width: number | null = null;
  length: number | null = null;
  perimeter: number | null = null;
  area: number | null = null;

  constructor(private rectangleCalculatorService: RectangleCalculatorService) {}

  calculatePerimeter(): void {
    if (this.width !== null && this.length !== null) {
      this.perimeter = this.rectangleCalculatorService.calculatePerimeter(
        this.width,
        this.length,
      );
    }
  }

  calculateArea(): void {
    if (this.width !== null && this.length !== null) {
      this.area = this.rectangleCalculatorService.calculateArea(
        this.width,
        this.length,
      );
    }
  }

  calculateAll(): void {
    this.calculatePerimeter();
    this.calculateArea();
  }
}
