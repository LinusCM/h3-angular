import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RectangleInputFormComponent } from './rectangle-input-form.component';

describe('RectangleInputFormComponent', () => {
  let component: RectangleInputFormComponent;
  let fixture: ComponentFixture<RectangleInputFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RectangleInputFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RectangleInputFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
