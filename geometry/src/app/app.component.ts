import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { SquareInputFormComponent } from "./components/square-input-form/square-input-form.component";
import { RectangleInputFormComponent } from "./components/rectangle-input-form/rectangle-input-form.component";

@Component({
  selector: "app-root",
  standalone: true,
  imports: [
    RouterOutlet,
    SquareInputFormComponent,
    RectangleInputFormComponent,
  ],
  templateUrl: "./app.component.html",
  styleUrl: "./app.component.scss",
})
export class AppComponent {
  title = "geometry";
}
