import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { DirectiveTableComponent } from "./components/directive-table/directive-table.component";
import { MaterialTableComponent } from "./components/material-table/material-table.component";

@Component({
  selector: "app-root",
  standalone: true,
  imports: [RouterOutlet, DirectiveTableComponent, MaterialTableComponent],
  templateUrl: "./app.component.html",
  styleUrl: "./app.component.scss",
})
export class AppComponent {
  title = "most-sold-electric-cars";
}
