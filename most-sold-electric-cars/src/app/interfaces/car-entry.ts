export interface CarEntry {
  rank: number | null;
  model: string | null;
  quantity: number | null;
  increasePercent: number | null;
}
