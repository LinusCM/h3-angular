import { Injectable } from "@angular/core";
import { CarEntry } from "../interfaces/car-entry";
import { BehaviorSubject, Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CarRankingsService {
  // We do not want anyone to update our list, so we keep our subject/entries private.
  private topElectricCars2022: Array<CarEntry> = [];
  private topElectricCarSubject$: Subject<CarEntry[]> = new BehaviorSubject<CarEntry[]>(
    this.topElectricCars2022,
  );

  // Can only be subscribed to, as the subject is now an observable.
  topElectricCars$: Observable<CarEntry[]> = this.topElectricCarSubject$.asObservable();

  constructor() {
    this.topElectricCars2022 = [
      { rank: 1, model: "Skoda Enyaq", quantity: 1044, increasePercent: 284 },
      { rank: 2, model: "Tesla Model Y", quantity: 989, increasePercent: 100 },
      { rank: 3, model: "Polestar 2", quantity: 836, increasePercent: 1990 },
      { rank: 4, model: "Audi Q4", quantity: 816, increasePercent: 586 },
      { rank: 5, model: "Ford Mustang Mach-E", quantity: 659, increasePercent: 1333 },
      { rank: 6, model: "Kia EV6", quantity: 520, increasePercent: 100 },
      { rank: 7, model: "VW ID.4", quantity: 458, increasePercent: -61 },
      { rank: 8, model: "Volvo XC40", quantity: 416, increasePercent: 100 },
      { rank: 9, model: "Hyundai Ioniq 5", quantity: 365, increasePercent: 100 },
      { rank: 10, model: "Hyundai Kona", quantity: 359, increasePercent: -24 },
      { rank: 11, model: "Tesla Model 3", quantity: 350, increasePercent: -68 },
      { rank: 12, model: "Kia Niro", quantity: 346, increasePercent: -16 },
      { rank: 13, model: "Peugeot 208", quantity: 330, increasePercent: 131 },
      { rank: 14, model: "VW ID.3", quantity: 329, increasePercent: -54 },
      { rank: 15, model: "Cupra Born", quantity: 298, increasePercent: 100 },
      { rank: 16, model: "Mercedes-Benz EQA", quantity: 289, increasePercent: 51 },
      { rank: 17, model: "VW Up", quantity: 229, increasePercent: 332 },
      { rank: 18, model: "VW ID.5", quantity: 226, increasePercent: 100 },
      { rank: 19, model: "Mercedes-Benz EQB", quantity: 224, increasePercent: 100 },
      { rank: 20, model: "Fiat 500", quantity: 202, increasePercent: -40 },
      { rank: 21, model: "Renault Zoe", quantity: 185, increasePercent: -18 },
      { rank: 22, model: "Peugeot 2008", quantity: 169, increasePercent: 42 },
      { rank: 23, model: "Audi E-tron", quantity: 168, increasePercent: 25 },
      { rank: 24, model: "Dacia Spring", quantity: 160, increasePercent: 5233 },
      { rank: 25, model: "BMW i4", quantity: 142, increasePercent: 100 },
    ];

    this.topElectricCarSubject$.next(this.topElectricCars2022);
  }
}
