import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { CarRankingsService } from "../../services/car-rankings.service";
import { CarEntry } from "../../interfaces/car-entry";
import { CommonModule } from "@angular/common";

@Component({
  selector: "app-directive-table",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./directive-table.component.html",
  styleUrl: "./directive-table.component.scss",
})
export class DirectiveTableComponent {
  topElectricCars$: Observable<CarEntry[]> | undefined;

  constructor(private carRankingService: CarRankingsService) {
    this.topElectricCars$ = this.carRankingService.topElectricCars$;
  }
}
