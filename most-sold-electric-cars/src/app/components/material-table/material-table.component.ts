import { Component } from "@angular/core";
import { CarRankingsService } from "../../services/car-rankings.service";
import { CarEntry } from "../../interfaces/car-entry";
import { MatTableDataSource, MatTableModule } from "@angular/material/table";

@Component({
  selector: "app-material-table",
  standalone: true,
  imports: [MatTableModule],
  templateUrl: "./material-table.component.html",
  styleUrl: "./material-table.component.scss",
})
export class MaterialTableComponent {
  carMatDataSource: MatTableDataSource<CarEntry> = new MatTableDataSource<CarEntry>();
  displayedColumns: string[] = ["rank", "model", "quantity", "increasePercent"];

  constructor(private carRankingService: CarRankingsService) {}

  ngOnInit(): void {
    this.carRankingService.topElectricCars$.subscribe((cars) => {
      this.carMatDataSource.data = cars;
    });
  }
}
